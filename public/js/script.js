$(document).ready(function () {
  console.log("ready!");
  $(".btn-nav").on("click tap", function () {
    $(".nav-content").toggleClass("showNav hideNav").removeClass("hidden");
    $(this).toggleClass("animated");
  });

  $(".carousel1").slick({
    asNavFor: ".carousel2",
    slidesToShow: 18,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
  });

  $(".carousel2").slick({
    asNavFor: ".carousel1",
    slidesToShow: 18,
    slidesToScroll: 1,
    centerMode: true,
    focusOnSelect: true,
    variableWidth: true,
  });
});
