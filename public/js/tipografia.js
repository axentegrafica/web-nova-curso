// ObtÃ©n el select y el textarea
const select = document.getElementById("tipografias");
const textarea = document.getElementById("vistatipo");
const size = document.getElementById("size");
const sizekallaikos = document.getElementById("sizekallaikos");
console.log("size", size);
size.addEventListener("change", function () {
  console.log("cambioNoRango");
  const fontSize = parseInt(size.value);
  textarea.style.fontSize = fontSize + "px";
});

sizekallaikos.addEventListener("change", function () {
  console.log("cambioNoRango");
  const fontSize = parseInt(sizekallaikos.value);
  const textareakallaikos = document.getElementById("vistatipo-kallaikos");
  textareakallaikos.style.fontSize = fontSize + "px";
});

// Agrega un 'event listener' al select que se activarÃ¡ cuando se cambie el valor
select.addEventListener("change", function () {
  // Obtiene la opciÃ³n seleccionada
  const seleccion = select.value;
  console.log(seleccion);

  // Actualiza el class del textarea para aplicar la tipografÃ­a seleccionada
  textarea.setAttribute("class", seleccion);
});

function addLoremIpsum() {
  const textarea = document.getElementById("vistatipo");
  const loremIpsum =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit sed dui vel sollicitudin. Nullam sit amet mauris mauris.";

  textarea.value = loremIpsum;
}
function resetText() {
  const textarea = document.getElementById("vistatipo");
  const loremIpsum = 'ABCDEFG!"12345!·*^¨-...';

  textarea.value = loremIpsum;
}

function addLoremIpsumkallaikos() {
  const textareakallaikos = document.getElementById("vistatipo-kallaikos");
  const loremIpsum =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit sed dui vel sollicitudin. Nullam sit amet mauris mauris.";

  textareakallaikos.value = loremIpsum;
}
function resetTextkallaikos() {
  const textareakallaikos = document.getElementById("vistatipo-kallaikos");
  const loremIpsum = 'ABCDEFG!"12345!·*^¨-...';

  textareakallaikos.value = loremIpsum;
}
